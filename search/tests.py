from django.test import TestCase
from django.urls import reverse

import time

from selenium import webdriver
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary
from selenium.webdriver.firefox.options import Options

# Create your tests here.
class SearchTestCase(TestCase):
	def setUp(self):
		binary  = FirefoxBinary('/usr/lib/firefox/firefox')
		firefox_options = Options()
		firefox_options.add_argument('--headless')
		self.browser = webdriver.Firefox(firefox_options = firefox_options, firefox_binary = binary)

	def test_search_render(self):
		result = self.client.get(reverse('index'))
		self.assertEqual(result.status_code, 200)

	def test_search_functional(self):
		# Opens browser
		self.browser.get('http://localhost:8000/')

		# Check if important components are here
		search_box = self.browser.find_elements_by_id('search_box')
		self.assertEqual(len(search_box), 1)
		search_btn = self.browser.find_elements_by_id('search_btn')
		self.assertEqual(len(search_btn), 1)
		search_box = search_box[0]
		search_btn = search_btn[0]

		# At first, the list should be empty
		book_table = self.browser.find_elements_by_id('book_table')
		self.assertEqual(len(book_table), 0)

		# Submit something
		search_box.send_keys('AJaX')
		search_btn.click()

		time.sleep(5)

		# The table should appear
		book_table = self.browser.find_elements_by_id('book_table')
		self.assertEqual(len(book_table), 1)
		# Table header should contain three parts
		cover_header = self.browser.find_elements_by_id('cover_header')
		title_header = self.browser.find_elements_by_id('title_header')
		author_header = self.browser.find_elements_by_id('author_header')
		self.assertEqual(len(cover_header), 1)
		self.assertEqual(len(title_header), 1)
		self.assertEqual(len(author_header), 1)

	def tearDown(self):
		self.browser.quit();